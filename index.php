<html>
  <head>
    <link rel="stylesheet" href="/assets/dist/semantic.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="/assets/css/stylesheet.css" media="screen" charset="utf-8">
     <link href="https://fonts.googleapis.com/css?family=Heebo:900" rel="stylesheet">

    <script type="text/javascript" src="/assets/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/dist/js/semantic.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    <link rel="icon" type="image/png" href="/assets/img/icon_logo.png">

    <title>Welcome to Vendo!</title>
  </head>

  <body>
    <!-- FIX UI -->
    <?php
      session_start();
      $success = 0;

      if(isset($_GET["success"])){
        $success = $_GET["success"];
        if($success == 0){
          session_destroy();
        }
        header("Location: index.php");
      }

      if(isset($_SESSION["success"])){
        $success = $_SESSION["success"];
      }
    ?>

    <?php if($success == 0 || $success == 1): ?>
      <div class="ui landpage-image">
        <?php  if($success == 1): ?>
          <div class='ui inverted red segment'>Login failed.</div>
        <?php endif; ?>
        <div class="ui three column grid">
          <div class="column"></div>
          <div class="column">
            <p class="center">
              <img src="/assets/img/icon_logo.png" id="icon_logo"/>
            </p>
            <p class="center" id="logo">
              VENDO
            </p>
          </div>
          <div class="column"></div>
          <div class="column"></div>
          <div class="column">
            <form class="ui inverted blue form segment" action="/views/application/login.php" method="post" id="login-form">
              <div class="field">
                <div class="ui left icon input">
                  <i class="user icon"></i>
                  <input type="text" name="username" placeholder="Username">
                </div>
              </div>
              <div class="field">
                <div class="ui left icon input">
                  <i class="lock icon"></i>
                  <input type="password" name="password" placeholder="Password">
                </div>
              </div>
              <input class="ui button" type="submit" value="Submit">
            </form>
          </div>
        </div>
        <div class="ui inverted blue footer segment">Icons made by <a href="http://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
      </div>
    <?php elseif($success == 2 || $success == 3):
      include 'views/navbar.php'; ?>
      <div class="body">
        <div class="ui segment">
          <h1 class="ui blue header">Welcome to Vendo!</h1>
          <p>
            Vendo is your friendly on the go, pay on the delivery system, which lets you just pick what you want, whenever you want, and where you want it to be. With Vendo, you don't have to think about how to pay it because you only have to pay it right at your doorstep, and if you don't like the item, you can always just give it back to us, free of charge!
          </p>
          <p>
            So, what are you waiting for? Order now!
          </p>
        </div>
      </div>
      <div class="ui inverted blue footer segment">A CS 165 Project. Icons made by <a href="http://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    <?php endif;?>
  </body>
</html>
