<?php
  $success = $_SESSION['success'];
  if($success == 2):
?>
  <nav class="ui inverted blue labeled icon menu" id="navbar">
    <a class="header item" href="/index.php">
      <div>
        <img src="/assets/img/white_icon_logo.png" alt="" />
      </div>
      <div class="vendo">VENDO</div>
    </a>
    <a class="item" href="/index.php"><i class="home icon"></i>HOME</a>
    <a class="item" href="/views/products/index.php"><i class="list icon"></i>PRODUCTS</a>
    <a class="item" href="/views/admins/view_users.php"><i class="user icon"></i>USERS</a>
    <a class="item" href="/index.php?success=0"><i class="sign out icon"></i>Logout</a>
    <div class="right menu">
      <div class="item">
        Welcome, <?php echo $_SESSION["admin_username"]?>
      </div>
    </div>
  </nav>
<?php elseif($success == 3): ?>
  <nav class="ui inverted blue labeled icon menu" id="navbar">
    <a class="header item" href="/index.php">
      <div>
        <img src="/assets/img/white_icon_logo.png" alt="" />
      </div>
      <div class="vendo">VENDO</div>
    </a>
    <a class="item" href="/index.php"><i class="home icon"></i>HOME</a>
    <a class="item" href="/views/products/index.php"><i class="list icon"></i>PRODUCTS</a>
    <a class="item" href="/views/regular_users/view_profile.php"><i class="user icon"></i>PROFILE</a>
    <a class="item" href="/index.php?success=0"><i class="sign out icon"></i>Logout</a>
    <div class="right menu">
      <div class="item">
        <a class="ui button" href="/views/products_contained/view_cart.php"><i class="cart icon"></i>VIEW CART</a>
      </div>
      <div class="item">
        Welcome, <?php echo $_SESSION["username"]?>
      </div>
    </div>
  </nav>
<?php endif?>
