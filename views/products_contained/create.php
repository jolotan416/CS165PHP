<?php
  session_start();
  include_once("../../controllers/products_contained.php");

  $products_contained_controller = new ProductContainedController();

  $user_id = $_GET['user_id'];
  $product_id = $_GET['product_id'];

  $success = $products_contained_controller->create($user_id, $product_id);
  if($_SESSION["success"] == 2){
    header("Location: /views/regular_users/view_profile.php?added=$success");
  }
  else{
    header("Location: /views/products/index.php?added=$success");
  }
?>
