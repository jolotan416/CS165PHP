<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="/assets/dist/semantic.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="/assets/css/stylesheet.css" media="screen" charset="utf-8">

    <script type="text/javascript" src="/assets/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/assets/dist/semantic.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    <link rel="icon" type="image/png" href="/assets/img/icon_logo.png">

    <title>Vendo Cart-Vendo</title>
  </head>
  <body>
    <?php
      session_start();

      if(!isset($_SESSION['user_id'])){
        header("Location: /index.php");
      }
      $role = $_SESSION['success'];
      $user_id = $_SESSION['user_id'];

      include_once('../../controllers/products_contained.php');
      include '../navbar.php';
      $product_contained_controller = new ProductContainedController();
      $cart = $product_contained_controller->view_cart($user_id);
      $total = 0;
    ?>
    <div class="body">
      <?php if(isset($_GET['checkout']) && $_GET['checkout'] == 1): ?>
        <div class="ui done modal">
          <h3 class="header">Pay On Delivery</h3>
          <div class="content">
            Please prepare the exact amount for the orders you have checked out or use your card for paying .
          </div>
        </div>
      <?php endif; ?>
      <?php if(isset($_GET['edited']) && $_GET['edited'] == 1): ?>
        <div class="ui inverted blue segment">
          Quantity Edited.
        </div>
      <?php endif; ?>
      <?php if(isset($_GET['deleted']) && $_GET['deleted'] == 1): ?>
        <div class="ui inverted blue segment">
          Orders Deleted.
        </div>
      <?php endif; ?>
      <h1 class="ui blue header">
        <a class="ui right floated checkout blue button" href="#"><i class="cart icon"></i>Checkout</a>
        My Cart
      </h1>
      <table class="ui selectable celled striped table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th colspan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php if(empty($cart)): ?>
            <tr>
              <td colspan="6" class="center aligned">
                You have currently not ordered any product.
              </td>
            </tr>
          <?php else: ?>
            <?php
              foreach ($cart as $product):
                $replace = array("P", "h", "p", ",", "$");
                $price = str_replace($replace, "", $product['price']);
                $purchase = str_replace($replace, "", $product['purchase']);
                $total += intval(str_replace($replace, "", $product['purchase']));
            ?>
            <tr>
              <td><?php echo $product['name']; ?></td>
              <td><?php echo "Php" . $price; ?></td>
              <td><?php echo $product['product_count']; ?></td>
              <td><?php echo "Php" . $purchase; ?></td>
              <td class="center aligned">
                <form class="ui form segment" action="/views/products_contained/edit.php" method="post">
                  <div class="two fields">
                    <input type="number" name="quantity" min=0 max="<?php echo $product['product_count']; ?>" value="<?php echo $product['product_count']; ?>">
                    <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>">
                    <input type="hidden" name="current" value="<?php echo $product['product_count']; ?>">
                    <input class="ui blue button" type="submit" value="Edit Quantity">
                  </div>
                </form>
              </td>
              <td class="center aligned">
                <a class="ui red button" href="/views/products_contained/delete.php?product_id=<?php echo $product['product_id']; ?>&&quantity=<?php echo $product['product_count']; ?>"><i class="cancel icon"></i>Cancel Orders</a>
              </td>
            </tr>
            <?php endforeach; ?>
            <tr>
              <td>Total</td>
              <td colspan="5" class="center aligned">
                <?php echo "Php$total.00"; ?>
              </td>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
      <div class="ui checkout small modal">
        <h3 class="header">Checkout Order</h3>
        <?php if($total != 0): ?>
          <div class="content">
            You're about to check out a total of <?php echo "Php$total.00"; ?>. Are you sure?
          </div>
          <div class="actions">
            <div class="ui blue approve button">Checkout</div>
            <div class="ui red deny button">Cancel</div>
          </div>
        <?php else: ?>
          <div class="content">
            You have no items in your cart.
          </div>
          <div class="actions">
            <div class="ui red deny button">Cancel</div>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="ui inverted blue footer segment">A CS 165 Project. Icons made by <a href="http://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
  </body>
</html>
