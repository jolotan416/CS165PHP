<?php
  session_start();
  include_once('../../controllers/products_contained.php');

  $product_contained_controller = new ProductContainedController();
  $success = $product_contained_controller->edit($_POST['product_id'], $_POST['quantity'], $_POST['current'], $_SESSION['user_id']);

  if($_SESSION['success'] == 2){
    header("Location: /views/regular_users/view_profile.php?edited=$success");
  }
  else{
    header("Location: /views/products_contained/view_cart.php?edited=$success");
  }
?>
