<?php
  session_start();
  include_once('../../controllers/products_contained.php');

  $product_contained_controller = new ProductContainedController();
  echo $_GET['product_id'] . " " . $_GET['quantity'] . " " . $_SESSION['user_id'];
  $success = $product_contained_controller->delete($_GET['product_id'], $_GET['quantity'], $_SESSION['user_id']);

  if($_SESSION['success'] == 2){
    header("Location: /views/regular_users/view_profile.php?deleted=$success");
  }
  else{
    header("Location: /views/products_contained/view_cart.php?deleted=$success");
  }
?>
