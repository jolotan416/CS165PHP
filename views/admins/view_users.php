<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="/assets/dist/semantic.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="/assets/css/stylesheet.css" media="screen" charset="utf-8">

    <script type="text/javascript" src="/assets/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/dist/js/semantic.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    <link rel="icon" type="image/png" href="/assets/img/icon_logo.png">

    <title>Users-Vendo</title>
  </head>
  <body>
    <?php
      session_start();

      if(!isset($_SESSION['success'])){
        header("Location: /index.php");
      }
      $role = $_SESSION['success'];
      $admin_id = $_SESSION['admin_id'];

      include_once('../../controllers/admins.php');
      include_once('../../controllers/regular_users.php');
      include '../navbar.php';

      $admin_controller = new AdminController;
      $regular_user_controller = new RegularUserController;

      $admins = $admin_controller->get_admins();
      $regular_users = $regular_user_controller->get_users();
    ?>
    <div class="body">
      <h1 class="ui blue header">
        Users
      </h1>
      <table class="ui selectable celled striped table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Username</th>
            <th>E-mail</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($regular_users as $regular_user): ?>
            <tr>
              <td><?php echo $regular_user['first_name'] . " " . $regular_user['middle_name'] . " " . $regular_user['last_name']; ?></td>
              <td><?php echo $regular_user['username']; ?></td>
              <td><?php echo $regular_user['email']; ?></td>
              <td class="center aligned"><a class="ui green button" href="/views/admins/go_to_profile.php?user_id=<?php echo $regular_user['user_id']; ?>"><i class="pin icon"></i>Show</a></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <div class="ui inverted blue footer segment">A CS 165 Project. Icons made by <a href="http://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
  </body>
</html>
