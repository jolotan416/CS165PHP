<?php
  session_start();
  include_once('../../controllers/regular_users.php');

  $regular_user_controller = new RegularUserController;

  $user_id = $_SESSION['user_id'];
  $username = $_SESSION['username'];
  if(!empty($_POST['old_password'])){
    $success = $regular_user_controller->check_password($username, $_POST['old_password']);
    if($success == 0){
      header("Location: /views/regular_users/view_profile.php?failed=1");
      return;
    }
    if(empty($_POST['current_password'])){
      header("Location: /views/regular_users/view_profile.php?failed=2");
      return;
    }
    if($_POST['current_password'] != $_POST['confirm_password']){
      header("Location: /views/regular_users/view_profile.php?failed=3");
      return;
    }
    $result = $regular_user_controller->change_password($user_id, $_POST['current_password']);
  }
  $success = $regular_user_controller->update_profile($user_id, $_POST['first_name'], $_POST['middle_name'], $_POST['last_name'], $_POST['email'], $_POST['address'], $_POST['contact_number']);
  if($success == 1){
    header("Location: /views/regular_users/view_profile.php");
  }
?>
