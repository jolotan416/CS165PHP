<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="/assets/dist/semantic.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="/assets/css/stylesheet.css" media="screen" charset="utf-8">

    <script type="text/javascript" src="/assets/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/assets/dist/semantic.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    <link rel="icon" type="image/png" href="/assets/img/icon_logo.png">

    <title>User Profile-Vendo</title>
  </head>
  <body>
    <?php
      session_start();

      if(!isset($_SESSION['user_id'])){
        header("Location: /index.php");
      }
      $role = $_SESSION['success'];
      $user_id = $_SESSION['user_id'];

      include_once('../../controllers/regular_users.php');
      include_once('../../controllers/orders.php');
      include_once('../../controllers/products_contained.php');
      include '../navbar.php';
      $regular_user_controller = new RegularUserController;
      $order_controller = new OrderController;
      $product_contained_controller = new ProductContainedController;

      $profile = $regular_user_controller->get_profile($user_id);
      $orders = $order_controller->get_orders($user_id);
    ?>
    <div class="body">
      <?php if(isset($_POST['edit']) || isset($_GET['failed'])): ?>
        <h1 class="ui blue header">User Profile</h1>
        <form class="ui blue form segment" action="/views/regular_users/edit_profile.php" method="post">
          <?php if(isset($_GET['failed']) && $_GET['failed']==1): ?>
            <div class="ui inverted red segment">
              Wrong Password. Try again.
            </div>
          <?php endif; ?>
          <?php if(isset($_GET['failed']) && $_GET['failed']==2): ?>
            <div class="ui inverted red segment">
              New Password is empty. Try again.
            </div>
          <?php endif; ?>
          <?php if(isset($_GET['failed']) && $_GET['failed']==3): ?>
            <div class="ui inverted red segment">
              New Password does not match. Try again.
            </div>
          <?php endif; ?>
          <div class="three fields">
            <div class="field">
              <label>First Name</label>
              <input type="text" name="first_name" value="<?php echo $profile['first_name'];?>">
            </div>
            <div class="field">
              <label>Middle Name</label>
              <input type="text" name="middle_name" value="<?php echo $profile['middle_name'];?>">
            </div>
            <div class="field">
              <label>Last Name</label>
              <input type="text" name="last_name" value="<?php echo $profile['last_name'];?>">
            </div>
          </div>
          <div class="field">
            <label>E-mail</label>
            <input type="text" name="email" value="<?php echo $profile['email'];?>">
          </div>
          <div class="field">
            <label>Address</label>
            <input type="text" name="address" value="<?php echo $profile['address'];?>">
          </div>
          <div class="field">
            <label>Contact Number (ex. 9171234567)</label>
            <input type="number" name="contact_number" value="<?php echo $profile['contact_number']; ?>">
          </div>
          <h4 class="ui header">Password (Leave blank, if unchanged)</h4>
          <div class="three fields">
            <div class="field">
              <label>Current Password</label>
              <input type="password" name="old_password">
            </div>
            <div class="field">
              <label>New Password</label>
              <input type="password" name="current_password">
            </div>
            <div class="field">
              <label>Confirm Password</label>
              <input type="password" name="confirm_password">
            </div>
          </div>
          <input class="ui button" type="submit" value="Submit Profile">
          <a class="ui red button" href="/views/regular_users/view_profile.php">Cancel</a>
        </form>
      <?php else: ?>
        <h1 class="ui blue header">
          <form action="view_profile.php" method="post">
            <input type="hidden" name="edit" value="1">
            <input type="submit" class="ui right floated blue button" value="Edit Profile">
          </form>
          User Profile
        </h1>
        <div class="ui blue segment">
          <h2 class="ui blue header"><?php echo $profile['first_name'] . " " . $profile['middle_name'] . " " . $profile['last_name'];?></h2>
          <p>
            <strong>E-mail: </strong><?php echo $profile['email']; ?>
          </p>
          <p>
            <strong>Address: </strong><?php echo $profile['address']; ?>
          </p>
          <p>
            <strong>Contact Number: </strong><?php echo $profile['contact_number']; ?>
          </p>
        </div>
      <?php endif; ?>
      <?php if($role == 2):
        include_once('../../controllers/products_contained.php');
        include_once('../../controllers/products.php');

        $product_contained_controller = new ProductContainedController();
        $product_controller = new ProductController();

        $cart = $product_contained_controller->view_cart($user_id);
        $products = $product_controller->get_products();

        $total = 0;
        $user_id = $_SESSION["user_id"];
      ?>
        <?php if((isset($_GET['added'])) && ($_GET['added'] == 1)):?>
          <div class="ui inverted blue segment">
            Added to Cart
          </div>
        <?php endif; ?>
        <?php if(isset($_GET['edited']) && $_GET['edited'] == 1): ?>
          <div class="ui inverted blue segment">
            Quantity Edited.
          </div>
        <?php endif; ?>
        <?php if(isset($_GET['deleted']) && $_GET['deleted'] == 1): ?>
          <div class="ui inverted blue segment">
            Orders Deleted.
          </div>
        <?php endif; ?>
        <h1 class="ui blue header">
          <div class="ui right floated checkout blue button" href="#"><i class="cart icon"></i>Checkout</div>
          <div class="ui right floated product blue button" href="#"><i class="add icon"></i>Add Items</div>
          Current Cart
        </h1>
        <table class="ui selectable celled striped table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Subtotal</th>
              <th colspan="2">Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php if(empty($cart)): ?>
              <tr>
                <td colspan="6" class="center aligned">
                  No products ordered
                </td>
              </tr>
            <?php else: ?>
              <?php
                foreach ($cart as $product):
                  $replace = array("P", "h", "p", ",", "$");
                  $price = str_replace($replace, "", $product['price']);
                  $purchase = str_replace($replace, "", $product['purchase']);
                  $total += intval(str_replace($replace, "", $product['purchase']));
              ?>
              <tr>
                <td><?php echo $product['name']; ?></td>
                <td><?php echo "Php" . $price; ?></td>
                <td><?php echo $product['product_count']; ?></td>
                <td><?php echo "Php" . $purchase; ?></td>
                <td class="center aligned">
                  <form class="ui form segment" action="/views/products_contained/edit.php" method="post">
                    <div class="two fields">
                      <input type="number" name="quantity" min=0 max="<?php echo $product['product_count']; ?>" value="<?php echo $product['product_count']; ?>">
                      <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>">
                      <input type="hidden" name="current" value="<?php echo $product['product_count']; ?>">
                      <input class="ui blue button" type="submit" value="Edit Quantity">
                    </div>
                  </form>
                </td>
                <td class="center aligned">
                  <a class="ui red button" href="/views/products_contained/delete.php?product_id=<?php echo $product['product_id']; ?>&&quantity=<?php echo $product['product_count']; ?>"><i class="cancel icon"></i>Cancel Orders</a>
                </td>
              </tr>
              <?php endforeach; ?>
              <tr>
                <td>Total</td>
                <td colspan="5" class="center aligned">
                  <?php echo "Php$total.00"; ?>
                </td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
        <div class="ui checkout small modal">
          <h3 class="header">Checkout Order</h3>
          <?php if($total != 0): ?>
            <div class="content">
              You're about to check out a total of <?php echo "Php$total.00"; ?> for <?php echo $_SESSION['username']; ?>. Are you sure?
            </div>
            <div class="actions">
              <div class="ui blue approve button">Checkout</div>
              <div class="ui red deny button">Cancel</div>
            </div>
          <?php else: ?>
            <div class="content">
              There are no items in the cart.
            </div>
            <div class="actions">
              <div class="ui red deny button">Cancel</div>
            </div>
          <?php endif; ?>
        </div>
        <div class="ui product fullscreen modal">
          <h3 class="header">Add Items</h3>
          <div class="content">
            <table class="ui selectable celled striped table">
              <thead>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th colspan="2">Actions</th>
              </thead>
              <tbody>
                <?php foreach ($products as $product):
                  $replace = array("P", "h", "p", ",", "$");
                  $price = str_replace($replace, "", $product['price']); ?>
                  <?php if ($product['quantity'] != 0): ?>
                    <tr>
                      <td><?php echo $product['name']; ?></td>
                      <td><?php echo $product['description']; ?></td>
                      <td><?php echo "Php" . $price; ?></td>
                      <td class="center aligned">
                        <a class="ui blue button" href="/views/products_contained/create.php?<?php echo 'user_id='. $user_id . '&product_id=' . $product['product_id']?>"><i class="cart icon"></i>Add to Cart</a>
                      </td>
                    </tr>
                  <?php endif; ?>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="actions">
            <div class="ui red deny button">Cancel</div>
          </div>
        </div>
      <?php endif; ?>
      <h1 class="ui blue header">Order History</h1>
      <div class="ui blue segment">
        <?php
        if(empty($orders)):
        ?>
          <p class="message">
            <strong>No check outs yet.</strong>
          </p>
        <?php
        else:
          $i = 0;
          foreach($orders as $order):
            $i += 1;
            $products = $product_contained_controller->get_products($order['order_id']);
        ?>
          <h3 class="ui blue header">Order #<?php echo $i; ?></h3>
          <p>
            <strong>Date Created: </strong><?php echo date("F d, Y, h:i a", strtotime($order['date_created'])); ?>
          </p>
          <?php if(isset($order['username'])): ?>
            <p>
              <strong>Created by: </strong><?php echo $order['username']; ?>
            </p>
          <?php endif; ?>
          <table class="ui selectable celled striped table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Quanttiy</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $total = 0;
              foreach($products as $product):
                $replace = array("P", "h", "p", ",", "$");
                $price = str_replace($replace, "", $product['price']);
                $purchase = str_replace($replace, "", $product['purchase']);
                $total += intval(str_replace($replace, "", $product['purchase']));
              ?>
                <tr>
                  <td><?php echo $product['name']; ?></td>
                  <td><?php echo "Php" . $price; ?></td>
                  <td><?php echo $product['product_count']; ?></td>
                  <td><?php echo "Php" . $purchase; ?></td>
                </tr>
              <?php endforeach; ?>
                <tr>
                  <td>Total</td>
                  <td colspan="3" class="center aligned">Php<?php echo $total; ?>.00</td>
                </tr>
            </tbody>
          </table>
        <?php
          endforeach;
        endif;
        ?>
      </div>
    </div>
    <div class="ui inverted blue footer segment">A CS 165 Project. Icons made by <a href="http://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
  </body>
</html>
