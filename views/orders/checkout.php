<?php
  session_start();
  include_once("../../controllers/orders.php");

  $order_controller = new OrderController;

  $user_id = $_SESSION["user_id"];
  $result = $order_controller->checkout($user_id);
  if($result == 1){
    if($_SESSION["success"] == 2){
      header("Location: /views/regular_users/view_profile.php?checkout=1");
    }
    else{
      header("Location: /views/products_contained/view_cart.php?checkout=1");
    }
  }
?>
