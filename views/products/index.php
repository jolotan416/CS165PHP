<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="/assets/dist/semantic.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="/assets/css/stylesheet.css" media="screen" charset="utf-8">

    <script type="text/javascript" src="/assets/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/dist/js/semantic.min.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>

    <link rel="icon" type="image/png" href="/assets/img/icon_logo.png">

    <title>Products-Vendo</title>
  </head>
  <body>
    <?php
      session_start();

      if(!isset($_SESSION['success'])){
        header("Location: /index.php");
      }
      $role = $_SESSION['success'];
      if($role == 2){
        $admin_id = $_SESSION['admin_id'];
      }
      else{
        $user_id = $_SESSION['user_id'];
      }

      include_once('../../controllers/products.php');
      include '../navbar.php';
      $product_controller = new ProductController();
      $products = $product_controller->get_products();
    ?>
    <div class="body">
      <?php if(isset($_GET['added'])){
        if($_GET['added'] == 1):?>
        <div class="ui inverted blue segment">
          Added to Cart
        </div>
      <?php elseif($_GET['added'] == 0): ?>
        <div class="ui inverted red segment">
          Out of Stock
        </div>
      <?php endif;}?>
      <h1 class="ui blue header">Products</h1>
      <table class="ui selectable celled striped table">
        <thead>
          <tr>
            <?php if($success == 2): ?>
              <th>Name</th>
              <th>Description</th>
              <th>Status</th>
              <th>Quantity</th>
              <th>Price</th>
            <?php elseif($success == 3): ?>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th colspan="2">Actions</th>
            <?php endif; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product):
            $replace = array("P", "h", "p", ",", "$");
            $price = str_replace($replace, "", $product['price']); ?>
            <?php if($success == 2): ?>
              <?php if ($product['quantity'] == 0): ?>
                <tr class="negative">
              <?php else: ?>
                <tr>
              <?php endif; ?>
                <td><?php echo $product['name']; ?></td>
                <td><?php echo $product['description']; ?></td>
                <td><?php echo $product['status']; ?></td>
                <td><?php echo $product['quantity']; ?></td>
                <td><?php echo "Php" . $price; ?></td>
              </tr>
            <?php elseif($success == 3): ?>
              <?php if ($product['quantity'] != 0): ?>
                <tr>
                  <td><?php echo $product['name']; ?></td>
                  <td><?php echo $product['description']; ?></td>
                  <td><?php echo "Php" . $price; ?></td>
                  <td class="center aligned">
                    <a class="ui blue button" href="/views/products_contained/create.php?<?php echo 'user_id='. $user_id . '&product_id=' . $product['product_id']?>"><i class="cart icon"></i>Add to Cart</a>
                  </td>
                </tr>
              <?php endif; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="ui inverted blue footer segment">A CS 165 Project. Icons made by <a href="http://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
  </body>
</html>
