<?php
include_once("../../models/Product.php");

class ProductController{
  private $product;

  public function __construct()
  {
    $this->product = new Product();
  }

  public function get_products()
  {
    $products = $this->product->get_products();
    return $products;
  }
}
?>
