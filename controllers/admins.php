<?php
  include_once("../../models/Admin.php");
  include_once("../../models/RegularUser.php");

  class AdminController{
    private $admin;

    public function __construct(){
      $this->admin = new Admin();
    }

    public function get_admins(){
      $admins = $this->admin->get_admins();
      return $admins;
    }

    public function go_to_profile($user_id){
      session_start();
      $_SESSION['user_id'] = $user_id;
      $regular_user = new RegularUser;
      $profile = $regular_user->get_profile($user_id);
      $_SESSION['username'] = $profile['username'];

      header("Location: /views/regular_users/view_profile.php");
    }
  }
?>
