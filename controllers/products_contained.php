<?php
include_once("../../models/ProductsContained.php");

class ProductContainedController{
  private $product_contained;

  public function __construct()
  {
    $this->product_contained = new ProductContained();
  }

  public function create($user_id, $product_id)
  {
    $result = $this->product_contained->create($user_id, $product_id);
    return $result;
  }

  public function view_cart($user_id)
  {
    $result = $this->product_contained->view_cart($user_id);
    return $result;
  }

  public function edit($product_id, $quantity, $current, $user_id)
  {
    $result = $this->product_contained->edit($product_id, $quantity, $current, $user_id);
    return $result;
  }

  public function delete($product_id, $quantity, $user_id)
  {
    $result = $this->product_contained->delete($product_id, $quantity, $user_id);
    return $result;
  }

  public function get_products($order_id)
  {
    $result = $this->product_contained->get_products($order_id);
    return $result;
  }
}
?>
