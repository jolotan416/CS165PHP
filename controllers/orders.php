<?php
  include_once("../../models/Order.php");

  class OrderController{
    private $order;

    public function __construct(){
      $this->order = new Order;
    }

    public function checkout($user_id){
      $result = $this->order->checkout($user_id);
      return $result;
    }

    public function get_orders($user_id){
      $result = $this->order->get_orders($user_id);
      return $result;
    }
  }
?>
