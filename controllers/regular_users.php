<?php
  include_once("../../models/RegularUser.php");

  class RegularUserController{
    private $regular_user;

    public function __construct()
    {
      $this->regular_user = new RegularUser();
    }

    public function get_users()
    {
      $result = $this->regular_user->get_users();
      return $result;
    }

    public function get_profile($user_id)
    {
      $result = $this->regular_user->get_profile($user_id);
      return $result;
    }

    public function check_password($username, $password)
    {
      $success = $this->regular_user->login($username, $password);
      if($success == -1){
        return 0;
      }
      return 1;
    }

    public function change_password($user_id, $password)
    {
      $success = $this->regular_user->change_password($user_id, $password);
      if($success == -1){
        return 0;
      }
      return 1;
    }

    public function update_profile($user_id, $first_name, $middle_name, $last_name, $email, $address, $contact_number)
    {
      $success = $this->regular_user->update_profile($user_id, $first_name, $middle_name, $last_name, $email, $address, $contact_number);
      if($success == -1){
        return 0;
      }
      return 1;
    }
  }
?>
