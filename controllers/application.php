<?php
session_start();

include_once("../../models/Admin.php");
include_once("../../models/RegularUser.php");

class ApplicationController{
  private $admin;
  private $regular_user;

  public function __construct()
  {
    $this->admin = new Admin();
    $this->regular_user = new RegularUser();
  }

  public function login($username, $password)
  {
    $success = $this->admin->login($username, $password);
    if($success == -1){
      $success = $this->regular_user->login($username, $password);
      if($success == -1){
        $_SESSION["success"] = 1;
        header("Location: /index.php");
        return;
      }
      $_SESSION["success"] = 3;
      $_SESSION["user_id"] = $success[0];
      $_SESSION["username"] = $success[1];
      header("Location: /index.php");
      return;
    }
    $_SESSION["success"] = 2;
    $_SESSION["admin_id"] = $success[0];
    $_SESSION["admin_username"] = $success[1];
    header("Location: /index.php");
  }
}
?>
