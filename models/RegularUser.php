<?php
include_once("Model.php");
class RegularUser extends Model{
  public function login($username, $password)
  {
    $sql = "SELECT username, user_id, password = crypt('$password', password) AS success FROM regular_users WHERE username = '$username';";
    $result = $this->connect_db($sql);
    if($result == 0){
      exit;
    }
    $row = pg_fetch_array($result, NULL, PGSQL_ASSOC);
    $data = $row['success'];
    //echo "$data <br>";

    if($data != 't'){
      return -1;
    }
    return array($row['user_id'], $row['username']);
  }

  public function get_users()
  {
    $sql = "SELECT * FROM regular_users ORDER BY first_name;";
    $result = $this->connect_db($sql);
    if($result == 0){
      return 0;
    }

    $users = pg_fetch_all($result);
    return $users;
  }

  public function get_profile($user_id)
  {
    $sql = "SELECT * FROM regular_users WHERE user_id = '$user_id';";
    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }

    $row = pg_fetch_array($result, NULL, PGSQL_ASSOC);
    return $row;
  }

  public function change_password($user_id, $password)
  {
    $sql = "UPDATE regular_users SET password = crypt('$password', gen_salt('md5'))
    WHERE user_id = $user_id;";
    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }
    return 1;
  }

  public function update_profile($user_id, $first_name, $middle_name, $last_name, $email, $address, $contact_number)
  {
    $sql = "UPDATE regular_users SET
    first_name = '$first_name',
    middle_name = '$middle_name',
    last_name = '$last_name',
    email = '$email',
    address = '$address',
    contact_number = $contact_number
    WHERE user_id = $user_id;";
    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }
    return 1;
  }
}
?>
