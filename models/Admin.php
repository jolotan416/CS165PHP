<?php
include_once("Model.php");
class Admin extends Model{
  public function login($username, $password)
  {
    $sql = "SELECT username, user_id, password = crypt('$password', password) AS success FROM admins WHERE username = '$username';";
    $result = $this->connect_db($sql);
    if($result == 0){
      exit;
    }
    $row = pg_fetch_array($result, NULL, PGSQL_ASSOC);
    $data = $row['success'];
    //echo "$data <br>";

    if($data != 't'){
      return -1;
    }
    return array($row['user_id'], $row['username']);
  }

  public function get_admins()
  {
    $sql = "SELECT * FROM admins ORDER BY first_name;";
    $result = $this->connect_db($sql);
    if($result == 0){
      return 0;
    }
    $admins = pg_fetch_all($result);
    return $admins;
  }
}
?>
