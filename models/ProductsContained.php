<?php
include_once("Model.php");
class ProductContained extends Model{
  public function create($user_id, $product_id)
  {
    $sql = "SELECT quantity FROM products WHERE product_id = $product_id";
    $result = $this->connect_db($sql);
    $row = pg_fetch_array($result, NULL, PGSQL_ASSOC);
    $data = $row['quantity'];
    if($data == 0){
      return 0;
    }
    $sql = "INSERT INTO products_contained(order_id, product_id) VALUES ((SELECT order_id FROM orders WHERE ordered_by = $user_id AND paid = false ORDER BY date_created DESC limit 1), $product_id);";
    $sql .= "UPDATE products SET quantity = $data - 1 WHERE product_id = $product_id;";

    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }

    $sql = "SELECT quantity FROM products WHERE product_id = $product_id";
    $result = $this->connect_db($sql);
    $row = pg_fetch_array($result, NULL, PGSQL_ASSOC);
    $data = $row['quantity'];
    if($data == 0){
      $sql = "UPDATE products SET status = 'Out of Stock' WHERE product_id = $product_id;";
      $result = $this->connect_db($sql);
    }
    return 1;
  }

  public function view_cart($user_id)
  {
    $sql = "SELECT products.product_id, name, price, product_count, (price * product_count) AS purchase
    FROM products, (SELECT order_id, product_id, COUNT(product_id) AS product_count
    FROM products_contained
    GROUP BY product_id, order_id
    HAVING order_id =
    (SELECT order_id FROM orders
    WHERE ordered_by = $user_id AND paid = false
    ORDER BY date_created DESC limit 1)) AS product_counts
    WHERE products.product_id = product_counts.product_id
    ORDER BY name;";

    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }

    $cart = pg_fetch_all($result);
    return $cart;
  }

  public function edit($product_id, $quantity, $current, $user_id)
  {
    $sql = "SELECT quantity FROM products WHERE product_id = $product_id";
    $result = $this->connect_db($sql);
    $row = pg_fetch_array($result, NULL, PGSQL_ASSOC);
    $data = $row['quantity'];

    $sql = "DELETE FROM products_contained
    WHERE product_id = $product_id
    AND order_id = (SELECT order_id FROM orders
    WHERE ordered_by = $user_id AND paid = false
    ORDER BY date_created DESC limit 1);";

    for($i=0; $i < $quantity; $i++){
      $sql .= "INSERT INTO products_contained (order_id, product_id)
      VALUES ((SELECT order_id FROM orders
      WHERE ordered_by = $user_id AND paid = false
      ORDER BY date_created DESC limit 1), $product_id);";
    }
    $sql .= "UPDATE products SET quantity = $data + ($current - $quantity), status = 'Available' WHERE product_id = $product_id";

    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }
    return 1;
  }

  public function delete($product_id, $quantity, $user_id)
  {
    $sql = "DELETE FROM products_contained
    WHERE product_id = $product_id
    AND order_id = (SELECT order_id FROM orders
    WHERE ordered_by = $user_id AND paid = false
    ORDER BY date_created DESC limit 1);";

    $sql .= "UPDATE products SET quantity = ((SELECT quantity FROM products WHERE product_id = $product_id) + $quantity), status = 'Available' WHERE product_id = $product_id;";

    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }
    return 1;
  }

  public function get_products($order_id)
  {
    $sql = "SELECT products.product_id, name, price, product_count, (price * product_count) AS purchase
    FROM products, (SELECT order_id, product_id, COUNT(product_id) AS product_count
    FROM products_contained
    GROUP BY product_id, order_id
    HAVING order_id = $order_id) AS product_counts
    WHERE products.product_id = product_counts.product_id
    ORDER BY name;";

    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }

    $products = pg_fetch_all($result);
    return $products;
  }
}
?>
