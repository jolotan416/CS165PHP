<?php
include_once("Model.php");
class Order extends Model{
  public function checkout($user_id){
    session_start();

    if($_SESSION['success'] == 2){
      $admin_id = $_SESSION['admin_id'];
      $sql = "UPDATE orders SET created_by = $admin_id,
      paid = true,
      date_created = now()
      WHERE ordered_by = $user_id
      AND paid = false;";
    }
    else{
      $sql = "UPDATE orders SET paid = true,
      date_created = now()
      WHERE ordered_by = $user_id
      AND paid = false;";
    }

    $sql .= "INSERT INTO orders(paid, ordered_by)
    VALUES (false, $user_id);";

    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }
    return 1;
  }

  public function get_orders($user_id){
    $sql = "SELECT order_id, date_created, paid, ordered_by, username FROM orders LEFT OUTER JOIN
    (SELECT * FROM admins) AS admin_details
    ON orders.created_by = admin_details.user_id
    WHERE ordered_by = '$user_id' AND paid = true;";
    $result = $this->connect_db($sql);
    if($result == 0){
      return -1;
    }
    $row = pg_fetch_all($result);
    return $row;
  }
}
?>
