<?php
include_once("Model.php");
class Product extends Model{
  public function get_products()
  {
    $sql = "SELECT * FROM products ORDER BY name;";
    $result = $this->connect_db($sql);
    if($result == 0){
      exit;
    }
    $products = pg_fetch_all($result);
    return $products;
  }
}
?>
