# An Inventory Project for CS 165 with the following features:
1. Log-in with username and password (passwords MUST NOT be stored in plaintext)
2. Post information on products being sold (item, description, status, number of items
available)- ADMIN
3. Shopping cart for products - USER (owner) & ADMIN
    * Add to cart
    * View cart
    * Update amount of existing cart entries
    * Delete items in cart
    * Submit cart items
4. View/Update of user profile with order history - USER(owner) & ADMIN
5. Detailed view of order history - USER(owner) & ADMIN
