INSERT INTO admins (first_name, middle_name, last_name, username, password, email)
VALUES ('John Louise', 'Hinayon', 'Tan', 'sample_admin', crypt('adminpass', gen_salt('md5')), 'jolo@gmail.com');

INSERT INTO regular_users (first_name, middle_name, last_name, username, password, email, address, contact_number, created_by)
VALUES ('Jacqueline', 'Mae', 'Tan', 'sample_user', crypt('userpass', gen_salt('md5')), 'jac@gmail.com', '124 Baker St. London Great Britain', 09171234567, 1);

INSERT INTO regular_users (first_name, middle_name, last_name, username, password, email, address, contact_number, created_by)
VALUES ('John', 'Henry', 'Smith', 'sample_user1', crypt('userpass', gen_salt('md5')), 'john@gmail.com', '124 Baker St. London Great Britain', 09171234567, 1);

INSERT INTO products (name, description, status, quantity, price)
VALUES ('Pringles', 'Saddle Chips', 'Available', 10, 100);

INSERT INTO products (name, description, status, quantity, price)
VALUES ('Nova', 'Ridge Chips', 'Out of Stock', 0, 30);

INSERT INTO products (name, description, status, quantity, price)
VALUES ('Cheetos', 'Cheesy Stick Chips', 'Available', 20, 80);

INSERT INTO orders (date_created, paid, ordered_by, created_by)
VALUES ('2016-11-15 04:05:06', TRUE, 1, 1);

INSERT INTO orders (paid, ordered_by)
VALUES (FALSE, 1);

INSERT INTO orders (paid, ordered_by)
VALUES (FALSE, 2);

INSERT INTO products_contained (order_id, product_id)
VALUES (1, 2);

INSERT INTO products_contained (order_id, product_id)
VALUES (1, 2);

INSERT INTO products_contained (order_id, product_id)
VALUES (1, 3);

INSERT INTO products_contained (order_id, product_id)
VALUES (2, 1);

INSERT INTO products_contained (order_id, product_id)
VALUES (3, 2);

INSERT INTO products_contained (order_id, product_id)
VALUES (3, 2);
