CREATE EXTENSION pgcrypto;

CREATE TABLE admins(
  user_id serial NOT NULL PRIMARY KEY,
  first_name varchar(50) NOT NULL,
  middle_name varchar(20) NOT NULL,
  last_name varchar(20) NOT NULL,
  username varchar(20) NOT NULL UNIQUE,
  password text NOT NULL,
  email varchar(20) NOT NULL UNIQUE
);

CREATE TABLE regular_users(
  user_id serial NOT NULL PRIMARY KEY,
  first_name varchar(50) NOT NULL,
  middle_name varchar(20) NOT NULL,
  last_name varchar(20) NOT NULL,
  username varchar(20) NOT NULL UNIQUE,
  password text NOT NULL,
  email varchar(20) NOT NULL UNIQUE,
  address varchar(50) NOT NULL,
  contact_number bigint NOT NULL,
  created_by integer NOT NULL references admins(user_id)
);

CREATE TABLE products(
  product_id serial NOT NULL PRIMARY KEY,
  name varchar(20) NOT NULL,
  description text,
  status varchar(20) NOT NULL,
  quantity integer NOT NULL,
  price money NOT NULL
);

CREATE TABLE orders(
  order_id serial NOT NULL PRIMARY KEY,
  date_created timestamp NOT NULL default now(),
  paid boolean NULL,
  ordered_by integer NOT NULL references regular_users(user_id),
  created_by integer NULL references admins(user_id)
);

CREATE TABLE products_contained(
  cid serial PRIMARY KEY,
  order_id integer NOT NULL references orders(order_id),
  product_id integer NOT NULL references products(product_id)
);
