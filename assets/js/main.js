$(function(){
  $('.ui.checkout.modal')
  .modal({
    onDeny    : function(){
      return true;
    },
    onApprove : function() {
      window.location.href="/views/orders/checkout.php";
    }
  })
  .modal("attach events", ".ui.checkout.button");

  $('.ui.done.modal').modal('show');

  $('.ui.product.modal')
  .modal({
    onDeny  : function(){
      return true;
    }
  })
  .modal("attach events", ".ui.product.button");
});
